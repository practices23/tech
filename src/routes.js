import React from 'react';

import Mision from './pages/mision';
import Vision from './pages/vision';
import Servicios from './pages/servicios';
import Contacto from './pages/contacto';
import IndexPage from './pages/indexPage';
import Page404 from './pages/page404';

import { Navigate, Route, Routes } from 'react-router-dom';

export default function Router() {

    return(
        <Routes>
            <Route path="/dashboard" element={<IndexPage />} />
            <Route path="/dashboard/mision" element={<Mision/>} />
            <Route path="/dashboard/vision" element={<Vision/>} />
            <Route path="/dashboard/servicios" element={<Servicios/>} />
            <Route path="/dashboard/contacto" element={<Contacto/>} />
            <Route path="/404" element={<Page404 />} />
            <Route path="/" element={<Navigate to="/dashboard" />} />
            <Route path="*" element={<Page404 />} />
        </Routes>
    )
}

