import { Container, Stack } from '@mui/material'
import React from 'react'
import AppBarPage from '../components/AppBar'
import CardComponent from '../components/CardComponent';
import { mision1, mision2, mision3 } from '../images/pathImage';

const Vision = () => {

    const cont = "Ser la empresa de referencia que implementa métodos y herramientas innovadoras y disruptivas.";

    return (
        <Container fixed> 
            <AppBarPage />
            <Stack spacing={2}>
            <CardComponent img={mision2} title={"Vision"} text={cont} />
                <img src = {mision1} alt="title" width="250px" height="150px" />
                <img src = {mision3} alt="title" width="250px" height="150px" />
            </Stack>
        </Container>
    )
}

export default Vision