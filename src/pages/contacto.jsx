import { Drafts } from '@mui/icons-material'
import { Avatar, Container, List, ListItem, ListItemAvatar, ListItemText } from '@mui/material'
import React from 'react'
import AppBarPage from '../components/AppBar'
import CardComponent from '../components/CardComponent'
import { imageIcon } from '../images/pathImage'

const Contacto = () => {

    const cont = <>  
                    Si estas interesado en alguno de estos servicios 
                    puedes contactarnos en el siguiente correo:
                    <List>
                        <ListItem >
                            <ListItemAvatar>
                                <Avatar>
                                    <Drafts />
                                </Avatar>
                            </ListItemAvatar>
                            <ListItemText primary="contacto@zpin.tech" />
                        </ListItem>
                    </List>
                </>

    return (
        <Container fixed> 
            <AppBarPage />
            <CardComponent img={imageIcon} title={"Contacto"} text={cont} />
        </Container>
    )
}

export default Contacto