import { Container } from '@mui/material'
import React from 'react'
import AppBarPage from '../components/AppBar'
import CardComponent from '../components/CardComponent'
import { imageIcon } from '../images/pathImage'

const IndexPage = () => {

    const cont = "Nuestro equipo multidisciplinario integra una combinación de profesionales con una gran experiencia en el sector público y privado de diversas industrias, así como por talento joven que se distingue por su pasión en aprender.";

    return (
        <Container fixed> 
            <AppBarPage />
            <CardComponent img={imageIcon} title={"¿Quienes somos?"} text={cont} />
        </Container>
    )
}

export default IndexPage