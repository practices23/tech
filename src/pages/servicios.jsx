import { Container, Stack, Typography } from '@mui/material'
import React from 'react'
import AppBarPage from '../components/AppBar'
import { Dimensionamiento, Administración, UX, Diseño } from '../images/pathImage'
import CardComponent from '../components/CardComponent'

const Servicios = () => {

    const service1 = "Aplicamos técnicas para entender de forma detallada la problemática que tu organización busca solucionar. Esto permite diseñar soluciones que se vinculen de forma directa a los problemas que tu organización se oriente. Así, aseguramos que nuestros clientes cumplan con el propósito de su organización.";
    const service2 = "El éxito de los proyectos se asocia con realizar una planeación y evaluación de la ejecución con herramientas que atiendan cada contexto del proyecto. De esta forma personalizamos el seguimiento de los proyectos y ayudamos a nuestros clientes a lograr los resultados planeados.";
    const service3 = "Utilizamos las mejores técnicas de investigación de experiencia de usuario adaptadas a tu itinerario y presupuesto.";
    const service4 = "Te brindamos asesoría para el montaje de tu emprendimiento.";

    return (
        <Container fixed> 
            <AppBarPage />
            <Stack spacing={2}>
                <Typography variant='h3'> 
                    Nuestros servicios
                </Typography>
                <p style={{textAlign:'center'}}>  
                    Proporcionamos soluciones administrativas y tecnológicas en esquema modular o general para personas morales o físicas
                </p>
                <CardComponent img={Dimensionamiento } title={"Dimensionamiento del problema"} text={service1} />
                <CardComponent img={Administración} title={"Administración de proyectos"} text={service2} />
                <CardComponent img={UX} title={"UX Research"} text={service3} />
                <CardComponent img={Diseño} title={"Diseño de Servicios"} text={service4} />
            </Stack>

        </Container>
    )
}

export default Servicios