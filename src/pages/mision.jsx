import { Container, Stack } from '@mui/material'
import React from 'react'
import AppBarPage from '../components/AppBar'
import CardComponent from '../components/CardComponent'
import { mision1, mision2, mision3 } from '../images/pathImage'

const Mision = () => {
    
    const conte= "Diseñar e implementar servicios y herramientas tecnológicas que ayuden a nuestros clientes a cumplir su propósito";

    return (
        <Container fixed> 
            <AppBarPage />
            <Stack spacing={2}>
            <CardComponent img={mision1} title={"Mision"} text={conte} />
                <img src = {mision2} alt="title" width="250px" height="150px" />
                <img src = {mision3} alt="title" width="250px" height="150px" />
            </Stack>
        </Container>
    )
}

export default Mision