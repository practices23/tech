import { Card, CardContent, CardHeader, Typography } from '@mui/material'
import React from 'react'

/**
 * 
 * @param { props.img }  img para mostrar
 * @param { props.title }  title titulo 
 * @param { props.text }  text contenido 
 * @returns 
 */
const CardComponent = (props) => {
    return (
        <Card>
            <CardHeader 
                avatar={<img src={ props.img } width="100px" height="100px" alt='imgicon' />}
                title={<Typography variant='h4'> {props.title} </Typography>}
            />
            <CardContent>
                <p style={{textAlign:'center'}}>
                    {props.text}
                </p>
            </CardContent>
        </Card>
    )
}

export default CardComponent