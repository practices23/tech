import { AppBar, Box, Button, Container, Menu, MenuItem, Stack } from '@mui/material';
import React, { useState } from 'react'
import MenuIcon from '@mui/icons-material/Menu';
import { Link as RouterLink } from 'react-router-dom';

const AppBarPage = () => {

    const [anchorEl, setAnchorEl] = useState(null);
    const open = Boolean(anchorEl);
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };
    
    return (
        <AppBar position="static">
            <Container fixed>
                <Box sx={{ flexGrow: 1,}}>
                    <Stack direction='row' spacing={2}>
                        <Button
                            id="basic-button"
                            aria-controls={open ? 'basic-menu' : undefined}
                            aria-haspopup="true"
                            aria-expanded={open ? 'true' : undefined}
                            onClick={handleClick}
                        >
                            <MenuIcon style={{color:'#fff'}} />
                        </Button>
                        <Menu
                            id="basic-menu"
                            anchorEl={anchorEl}
                            open={open}
                            onClose={handleClose}
                            MenuListProps={{
                            'aria-labelledby': 'basic-button',
                            }}
                        >
                            {pages.map(index => 
                                <MenuItem to={index.url} component={RouterLink} > {index.name}</MenuItem>
                            )}
                        </Menu>
                    </Stack>
                </Box>

            </Container>
        </AppBar>
    );

}

const pages=[
    {url:"/", name:"Inicio" },
    {url:"/dashboard/mision", name:"Nuestra Mision"},
    {url:"/dashboard/vision", name:"Nuestra Vision"},
    {url:"/dashboard/servicios", name:"Nuestros Servicios"},
    {url:"/dashboard/contacto", name:"Contacto"},
]



export default AppBarPage